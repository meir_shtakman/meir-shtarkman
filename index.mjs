import fsp from 'fs/promises'
import fs from 'fs'
import readline from 'readline'
const readLine = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });
let tasks = [];


function printMenu(){
    readLine.question(` Task List, please chose one of the following options:
    1. Add task
    2. Delete task
    3. Edit Task
    0. Exit`, input =>{
        console.log(input);
    })
}

function init(){

}

async function readData(){
    if(fs.existsSync('./data.json')){
    const data = await fsp.readFile('./data.json');
    tasks = JSON.parse(data);
    }
}

function saveData(){
    fsp.writeFile(JSON.stringify(tasks));
}

function runApp(){
    printMenu();

}

runApp()
runApp()