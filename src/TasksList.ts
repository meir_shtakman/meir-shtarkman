import Task from "./Task.js";


export default class TaskList{
    list: Task[];
    constructor(list:Task[]){
        this.list = [...list];
    }

    updateTaskStatus(id:string,indexStr:string,filter:string){
        let index: number = Number(indexStr); 
        if(id !== undefined){
        this.list.forEach(task => {
            if(task.id === id){
                task.active = !task.active;
            }
        });
        } else if(!isNaN(parseInt(indexStr))) {
            let filteredList = this.getFilteredList(filter);
            filteredList[index].active = !filteredList[index].active;
        } else{
            throw new Error("Invalid input");
        }
    }

    deleteTask = (id:string,indexStr:string,filter:string)=> {
        if(id === undefined){
            id = this.getIdFromFilteredListUsingIndex(indexStr,filter);
        }
        let index = this.list.findIndex(task => task.id === id);
        this.list.splice(index,1);
    }

    deleteAllCompletedTask = ()=>{
        this.list = this.list.filter(task => task.active)
    }

    getFilteredList(filter:string){
            switch (filter) {
                case 'active':
                    return this.list.filter(task => task.active)
                case 'complete':
                    return this.list.filter(task => !task.active)
                default:
                return this.list;
            }
    }

    getIdFromFilteredListUsingIndex(indexStr:string,filter:string){
        let index = parseInt(indexStr);
        let filteredList = this.getFilteredList(filter);
        return filteredList[index].id;
    }

}