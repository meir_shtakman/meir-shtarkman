import log from '@ajar/marker'
import Task from './Task.js'

export function print(msg:string){
    log.green(msg)
}

export function error(msg:string){
    log.red(msg)
}

export function printTable(list:Task[],filter:string){
    switch (filter) {
        case 'active':
            console.table(list.filter(task => task.active));
            break;
        case 'complete':
            console.table(list.filter(task => !task.active));
            break;
        default:
            console.table(list)
        }
}

export function help(){
    log.yellow(`here are the legals commands and prameters:
    1. create ||| prameter required: -title=""; 
    2. read   ||| optional filters: --active, --complete; 
    3. update ||| prameter required: -id=""; 
    4. delete ||| prameter required: -id=""; 
    5. delete-all`)
}