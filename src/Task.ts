import { makeid } from "./utilities.js";

export default class Task{
    title: string;
    id:string;
    active:boolean
 
    constructor(title: string) {
      this.title = title;
      this.id= makeid(10);
      this.active = true;
    }

}


