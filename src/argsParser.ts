

export function getCommand(args: string[]): string{
    let command:string|undefined = args.find(arg=> arg === "create"|| arg === "read"||
    arg === "update"|| arg === "delete"|| arg === 'delete-all')
    if(command === undefined){
        throw new Error(`No legal command insert`)
    } else {
        return command;
    }
}

export interface ParametersObj{
    [index:string]:string;
}

export function getParameters(args: string[]){
    let parameters:ParametersObj = {};
    args.forEach(element => {
        if(element[0] === '-' && (element[1] !== '-' && element[1] !== '=')){
            let index = element.indexOf('=')
            parameters[element.substring(1,index)] = element.substring(index+1);
        }
    });

    return parameters;
}


export function getFilter(args: string[]): string{
    let filterString:undefined|string= args.find(arg=> arg === "--all"|| arg === "--complete"||
    arg === "--active")
    if(filterString === undefined){
        return "all"
    } else {
        return filterString.substring(2);
    }
}

