
import * as parser from './argsParser.js'

import TaskList from "./TasksList.js";
import * as dataStorage from './dataStorageHandler.js';
import * as ui from './uiManager.js'
import doCommand from './executeCommand.js'

init();

async function init(){
    try{
        let tasks: TaskList = new TaskList(JSON.parse(await dataStorage.readData()).list);
        const myArgs: string[] = process.argv.slice(2);
   
        if(myArgs.includes('help')){
            throw Error('Help needed:')
        }
        let command: string =  parser.getCommand(myArgs);
        let parameters: parser.ParametersObj = parser.getParameters(myArgs);
        let filter:string = parser.getFilter(myArgs);
        doCommand(tasks,command,parameters,filter);
    } catch(err) {
        ui.error(err as string)
        ui.help();
    }
    
}






