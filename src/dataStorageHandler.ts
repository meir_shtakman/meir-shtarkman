import fsp from 'fs/promises';
import fs from 'fs';
import TaskList from "./TasksList.js";


export function saveData(data:string){
    fsp.writeFile('./data.json',data);
}

export async function readData(): Promise<string>{
    if(fs.existsSync('./data.json')){
    const data = await fsp.readFile('./data.json',"utf8");
    return data
    } else {
        saveData(JSON.stringify(new TaskList([])));
        return JSON.stringify(new TaskList([]));
    }
}    