import Task from "./Task.js";
import * as parser from './argsParser.js'

import TaskList from "./TasksList.js";
import * as dataStorage from './dataStorageHandler.js';
import * as ui from './uiManager.js'

export default function doCommand(tasks:TaskList,command:string,parameters:parser.ParametersObj,filter:string){
    console.log(tasks, {command},{parameters})
    switch (command) {
        case 'create':
            if(parameters.title){
                tasks.list.push(new Task(parameters.title));
            } else {
                ui.error("must enter title when creating new task")
            }
            
            break;
        case 'read':
            ui.printTable(tasks.list,filter);
            break;
        case 'update':
            try{
                tasks.updateTaskStatus(parameters.id,parameters.index,filter);
            } catch(err) {
                ui.error(err as string);
            }
            break;
        case 'delete':
            try{
                tasks.deleteTask(parameters.id,parameters.index,filter);
            } catch(err) {
                ui.error(err as string);
            }
            break;
            case 'delete-all':
                try{
                    tasks.deleteAllCompletedTask();
                } catch(err) {
                    ui.error(err as string);
                }
                break;    
        default:
            ui.error('Sorry, that is not something I know how to do.');
        }
    dataStorage.saveData(JSON.stringify(tasks));    
}

